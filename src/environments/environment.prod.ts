export const environment = {
  production: true,
  baseUrl: `dev01-app.hlrpphoenix.com/`,
  eventBaseUrl: `https://dev01-app.hlrpphoenix.com/capture`
};
