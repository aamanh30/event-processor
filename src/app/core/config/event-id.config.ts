export const CoursePointEventIds = {
  assignmentsPage: 'b30283a6-8080-48f6-8a82-d2afafad7650',
  classesPage: 'b30283a6-8080-48f6-8a82-d2afafad7651',
  contentPage: 'b30283a6-8080-48f6-8a82-d2afafad7652',
  dashoardPage: 'b30283a6-8080-48f6-8a82-d2afafad7653',
  gradebookPage: 'b30283a6-8080-48f6-8a82-d2afafad7654',
  overviewPage: 'b30283a6-8080-48f6-8a82-d2afafad7655',
  resultsPage: 'b30283a6-8080-48f6-8a82-d2afafad7656',
  rosterPage: 'b30283a6-8080-48f6-8a82-d2afafad7657',
};
