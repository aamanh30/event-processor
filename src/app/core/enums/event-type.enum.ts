export enum EventType {
    onLoad = 'onLoad',
    onClick = 'onClick'
}

export enum DOMEventTypes {
    click,
    change,
    keyup,
    keypress
}