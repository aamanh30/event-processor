export enum RouteSlug {
    assignments = 'assignments',
    classes = 'classes',
    content = 'content',
    dashboard = 'dashboard',
    gradebook = 'gradebook',
    overview = 'overview',
    results = 'results',
    roster = 'roster'
}